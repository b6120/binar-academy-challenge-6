const express = require('express');
const router = express.Router();
const Index = require("../controllers/indexController");
const { route } = require('./adminRoute');

router.get("/", Index.getIndexPage)

module.exports = router