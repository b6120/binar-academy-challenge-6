const express = require("express");
const router = express.Router();

const userAPI = require("../controllers/userApiController");

const userApi = new userAPI;

router.delete("/delete/:id", userApi.deleteUser);
router.get("/restore/:id", userApi.restoreUser)

module.exports = router
