const router = require("express").Router();
const GameController = require("../controllers/gameController")

router.get("/", GameController.getGamePage);

module.exports = router