const express = require("express");
const router = express.Router();

// const adminAuth = require("./middleware/authentication");
const adminAuth = require("../middleware/authentication");
const AdminController = require("../controllers/adminController")

const adminController = new AdminController
router.get("/", /*adminAuth,*/ adminController.getDashboard)
router.get("/create", adminAuth, adminController.getCreatePage)
router.post("/create", adminAuth, adminController.addNewPlayer)
router.get("/delete/:id", adminAuth, adminController.deletePlayer)
router.get("/edit/:id", adminAuth, adminController.getUpdatePage)
router.post("/edit/:id", adminAuth, adminController.updatePlayer)
module.exports = router