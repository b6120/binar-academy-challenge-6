const express = require("express");
const router = express.Router();

const UserController = require("../controllers/userController")
const userController = new UserController;

router.get("/signup", userController.getSignupPage)
router.post("/signup", userController.signupNewUser)
router.get("/login", userController.getLoginPage)
router.post("/login", userController.loginUser)
router.get("/logout", userController.logoutUser)


module.exports = router