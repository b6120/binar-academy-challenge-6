const express = require("express");
const sessions = require("express-session");
const bodyParser = require("body-parser");
const app = express();
const port = 3000;

const Model = require("./models");
const { User, Bio, UserHistory } = Model;

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));

// parse application/json
app.use(bodyParser.json());

//set view engine ejs
app.set("view engine", "ejs");
//static file use
app.use(express.static(__dirname + "/public"));

// creating 24 hours from milliseconds
const oneDay = 1000 * 60 * 60 * 24;

//session middleware
app.use(
  sessions({
    secret: "apaan ya gatau",
    saveUninitialized: true,
    cookie: { maxAge: oneDay },
    resave: false,
  })
);

const indexRouter = require("./routes/indexRoute")
const adminRouter = require("./routes/adminRoute");
const userRouter = require("./routes/userRouter");
const apiRouter = require("./routes/userApiRoute");
const gameRouter = require("./routes/gameRoute")

app.use("/", indexRouter)
app.use("/admin", adminRouter);
app.use("/user", userRouter);
app.use("/api/user", apiRouter);
app.use("/game", gameRouter)

app.get("/users", (req, res) => {
  User.findAll({
    include: {
      model: Bio,
      as: "biodata",
    },
  })
    .then((users) => {
      res.json({
        allUser: users,
      });
    })
    .catch((err) => {
      console.log(err);
      return res.status(500).json({ error: err.message });
    });
});


app.delete("/users/:id", (req, res) => {
  const id = req.params.id;
  User.destroy({
    where: { id: id },
    // force: true
  })
    .then((data) => {
      res.json({
        deletedUser: data,
      });
    })
    .catch((err) => {
      res.status(500).json({
        error: "something went wrong",
        message: err.message,
      });
    });
});

app.listen(port, async () => {
  console.log(`app is running on port ${port}`);
});
