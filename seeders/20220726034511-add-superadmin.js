'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
 
    await queryInterface.bulkInsert('game_users', [
      {
        id: "b79abcea-0507-47e3-a3f4-2c1c9604f653",
        user_name: 'fas',
        password: '123456',
        is_admin: true,
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        id: "1b97ad69-0c59-446b-952d-2b8c12419d94",
        user_name: 'bambang',
        password: '666',
        is_admin: false,
        created_at: new Date(),
        updated_at: new Date()
      }
    ], {});

    await queryInterface.bulkInsert('game_user_bio', [
      {
        first_name: 'farhan',
        last_name: 'arya',
        email: 'farhan@email.com',
        user_id: "b79abcea-0507-47e3-a3f4-2c1c9604f653",
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        first_name: 'bambang',
        last_name: 'tayo',
        email: 'bambang@email.com',
        user_id: "1b97ad69-0c59-446b-952d-2b8c12419d94",
        created_at: new Date(),
        updated_at: new Date()
      }
    ], {});
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */

     await queryInterface.bulkDelete('game_users', null, {});
     await queryInterface.bulkDelete('game_user_bio', null, {});
  }
};
