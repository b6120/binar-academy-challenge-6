const { DATE } = require("sequelize");
const Model = require("../models");
const { User, Bio, UserHistory } = Model;

class userAPI {
  constructor() {}

  deleteUser(req, res) {
    const id = req.params.id;
    User.destroy({
      where: { id: id },
      // force: true
    })
      .then((data) => {
        res.json({
          deletedUser: data,
        });
      })
      .catch((err) => {
        res.status(500).json({
          error: "something went wrong",
          message: err.message,
        });
      });
  }

  async restoreUser(req, res) {
    let userId = req.params.id;
    let timeNow = new Date().getTime()
    try {
        let editedUser = await User.findOne({
                where: {id: userId},
                paranoid: false
            }
          )
        await editedUser.update({
            deletedAt: null,
            updatedAt: timeNow
        })
        res.json({
            editedUser
        })
    }
    catch (err) {
        res.json({
            err: err.message
        })
    }
   
    //   .then(() => {
    //     User.update(
    //       {
    //         updatedAt: timeNow
    //       },
    //       {
    //         where: {
    //           id: userId,
    //         },
    //       }
    //     )
    //     User.save()
    //   })
    //   .then((data)=> {
    //     res.json({
    //         message: "sukses",
    //         data: data
    //     })
    //   })
    //   .catch(err => {
    //     res.json({ err: err.message })
    //   })

    // .then((data) => {
    //     res.json({
    //         data
    //     })
    // })
  }
}

module.exports = userAPI;
