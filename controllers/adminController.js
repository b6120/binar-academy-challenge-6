const Model = require("../models");
const { User, Bio, UserHistory } = Model;

class AdminController {
  constructor() {}

  getDashboard(req, res) {
    const errorMessage = req.query.error;
    let page = 0;
    const limit = 5;
    let offset = 0;

    if (!req.query.page) {
      page = 0;
      offset = page * limit;
    } else {
      page = req.query.page * 1;
      offset = (page - 1) * limit;
    }

    User.findAndCountAll({
      include: {
        model: Bio,
        as: "biodata",
      },
      offset: offset,
      limit: limit,
    })
      .then((users) => {
        const userCount = users.count
        page = Math.ceil(userCount/limit)
        res.render("dashboard", {
          title: "admin dashboard",
          users: users.rows,
          errorMessage,
          offset,
          page,
          userCount,
          admin: req.session.user.toUpperCase(),
        });
      })
      .catch((err) => {
        console.log(err);
        return res.status(500).json({ error: err.message });
      });
  }

  getCreatePage(req, res) {
    res.render("createUser", {
      title: "create player",
    });
  }

  addNewPlayer(req, res) {
    const { email, username, password, firstName, lastName, isAdmin } =
      req.body;
    User.create({
      userName: username,
      password,
      isAdmin,
    })
      .then((user) => {
        Bio.create({
          userId: user.id,
          email,
          firstName,
          lastName,
        });
      })
      .then(() => {
        res.redirect("/admin");
      })
      .catch((err) => {
        let string = encodeURIComponent("create user failed");
        res.redirect("/admin?error=" + string);
      });
  }
  deletePlayer(req, res) {
    let userId = req.params.id;
    User.destroy({
      where: {
        id: userId,
      },
    }).then((data) => {
      res.redirect("/admin");
    });
  }

  getUpdatePage(req, res) {
    User.findOne({
      include: {
        model: Bio,
        as: "biodata",
      },
      where: {
        id: req.params.id,
      },
    })
      .then((data) => {
        res.render("updatePage", {
          title: "edit page",
          data,
        });
      })
      .catch((err) => {
        res.json({
          message: err.message,
        });
      });
  }

  updatePlayer(req, res) {
    const userId = req.params.id;
    const { email, userName, password, firstName, lastName, isAdmin } =
      req.body;
    User.update(
      {
        userName,
        password,
        isAdmin,
        updatedAt: Date.now(),
      },
      {
        where: {
          id: userId,
        },
      }
    )
      .then(() => {
        Bio.update(
          {
            firstName,
            lastName,
            email,
            updatedAt: Date.now(),
          },
          {
            where: {
              userId: userId,
            },
          }
        );
      })
      .then(() => {
        res.redirect("/admin");
      })
      .catch((err) => {
        res.json({
          err: err.message,
        });
      });
  }
}

module.exports = AdminController;
