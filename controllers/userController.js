const Model = require("../models");
const {Sequelize, User, Bio, UserHistory} = Model

class UserController {
  constructor() {}

  getLoginPage(req, res) {
    const status = req.query.status
    res.render("login", { title: "login page", message: status });
  }

  getSignupPage(req, res) {
    res.render("signup", { title: "signup page", message: "you are not a user yet, please register"} )
  }

  signupNewUser (req, res) {
    const {userName, email, password, password2} = req.body;
    console.log(req.body)
    if(password !== password2) {
      res.render("signup", {title: "signup page", message: "your password doesnt match, please try again"})
    }
    else {
      User.create({
        userName,
        password,
        isAdmin: false
      })
      .then((data)=> {
        Bio.create({
          userId: data.id,
          email
        })
      })
      .then(()=> {
        let status = encodeURIComponent("create user complete, now please login");
        res.redirect("/user/login?status=" + status)
      })
      .catch(err => {
        res.render("signup", {title: "signup page", message: err.message})
      })
    }
   
  }
  
  loginUser(req, res) {
    const { username, password } = req.body;
    User.findOne({
      where: {
        userName: username,
      }
    })
    .then(loginUser => {
        if (loginUser === null) {
          res.redirect("/user/signup")
      } else if (loginUser.password !== password) {
        res.render("login", {
          title: "login error",
          message: "password is incorrect, pleaser try again",
        });
      } else if (loginUser.isAdmin === true) {
        // regenerate the session, which is good practice to help
        // guard against forms of session fixation
        req.session.regenerate(function (err) {
          if (err) next(err);
  
          // store user information in session, typically a user id
          req.session.user = loginUser.userName;
  
          // save the session before redirection to ensure page
          // load does not happen before session is saved
          req.session.save(function (err) {
            if (err) return next(err);
            res.redirect("/admin");
          });
        });
      } else {
        res.redirect("/game");
      }
    })
    .catch (err => {
      console.log(err)
    });
}

  logoutUser (req, res) {
     // logout logic

  // clear the user from the session object and save.
  // this will ensure that re-using the old session id
  // does not have a logged in user
  req.session.user = null;
  req.session.save(function (err) {
    if (err) next(err);

    // regenerate the session, which is good practice to help
    // guard against forms of session fixation
    req.session.regenerate(function (err) {
      if (err) next(err);
      res.redirect("/");
    });
  });
  }
}


        
module.exports = UserController;