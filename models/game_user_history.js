'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class UserHistory extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      models.UserHistory.belongsTo(models.User, {
        foreignKey: "user_id",
        // onDelete: "CASCADE",
        // onUpdate: "CASCADE",
      })
    }
  }
  UserHistory.init({
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    user_Id: { 
      type: DataTypes.UUID, 
      field: "user_id" 
    },
    start_time: {
      type: DataTypes.DATE,
      allowNull: false,
      
    },
    end_time: {
      type: DataTypes.DATE
    },
    is_win: {
      type: DataTypes.BOOLEAN
    },
    createdAt: {
      field: "created_at",
      allowNull: false,
      type: DataTypes.DATE,
      defaultValue: DataTypes.NOW
    },
    updatedAt: {
      field: "updated_at",
      type: DataTypes.DATE,
      defaultValue: DataTypes.NOW
    },
    deletedAt: {
      type: DataTypes.DATE,
      field: 'deleted_at'
    }
  }, {
    sequelize,
    modelName: 'UserHistory',
    tableName: 'game_user_history',
    paranoid: true,
    deletedAt: 'deleted_at'
  });
  return UserHistory;
};